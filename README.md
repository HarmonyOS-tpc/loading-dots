# LoadingDots

Customizable bouncing dots view for smooth loading effect. Mostly used in chat bubbles to indicate the other person is typing.

# Features
* LoadingDots animated view
* Use in xml
* Customize dots appearance
* Customize animation behavior
* Customize animation duration

# Usage Instructions

1. For basic usage, simply add to layout xml:

            <com.eyalbira.loadingdots.LoadingDots
                ohos:height="match_parent"
                ohos:width="match_content"/>

 2. To customize, simply use the needed Component attributes:

                <com.eyalbira.loadingdots.LoadingDots
                ohos:height="match_parent"
                ohos:width="match_content"
                app:LoadingDots_dots_count="4"
                app:LoadingDots_dots_size="3dp"
                app:LoadingDots_dots_space="1dp"
                app:LoadingDots_loop_duration="800"
                app:LoadingDots_loop_start_delay="100"
                app:LoadingDots_jump_duration="200"
                app:LoadingDots_jump_height="4dp"/>

3. Using programming syntax
    
            LoadingDots loadingDots = new LoadingDots(getContext());
            loadingDots.setAutoPlay(true);
            loadingDots.setDotsCount(DOTS_COUNT);
            loadingDots.setDotsColor(Color.RED.getValue());

# Installation Instructions

1.For using loading_dots module in a sample application, include the below library dependency

    Modify entry build.gradle as below :
    dependencies {
            compile project(':loading_dots')
    }

2.For using loading_dots module in a separate application, make sure to add the "loading_dots.har" in libs folder of "entry" module.

    Modify entry build.gradle as below :
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.har', '*.jar'])
    }

3.For using loading_dots from the remote repository, add the below dependency in "entry" build.gradle.

    Modify entry build.gradle as below :
    dependencies {
        implementation 'io.openharmony.tpc.thirdlib:loading-dots:1.0.0'
    }