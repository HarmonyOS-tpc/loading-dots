/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eyalbira.example.slice;

import com.eyalbira.example.ResourceTable;
import com.eyalbira.loadingdots.LoadingDots;
import com.eyalbira.loadingdots.utils.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final int DOTS_COUNT = 5;
    private Button start = null;
    private Button stop = null;
    private Button dynamic = null;
    private ComponentContainer root = null;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initLayout();
        initListeners();
    }

    private void initLayout() {
        start = (Button) findComponentById(ResourceTable.Id_start);
        stop = (Button) findComponentById(ResourceTable.Id_stop);
        dynamic = (Button) findComponentById(ResourceTable.Id_loaddynamic);
        root = (ComponentContainer) findComponentById(ResourceTable.Id_root);
    }

    private void initListeners() {
        start.setClickedListener(this);
        stop.setClickedListener(this);
        dynamic.setClickedListener(this);
    }

    private void startAll(ComponentContainer root) {
        int count = root.getChildCount();
        for (int index = 0; index < count; index++) {
            Component component = root.getComponentAt(index);
            if (component instanceof LoadingDots) {
                ((LoadingDots)component).startAnimation();
            }
            if (component instanceof ComponentContainer) {
                startAll((ComponentContainer) component);
            }
        }
    }

    private void stopAll(ComponentContainer root) {
        int count = root.getChildCount();
        for (int index = 0; index < count; index++) {
            Component component = root.getComponentAt(index);
            if (component instanceof LoadingDots) {
                ((LoadingDots)component).stopAnimation();
            }
            if (component instanceof ComponentContainer) {
                stopAll((ComponentContainer) component);
            }
        }
    }

    private void loadDynamic(ComponentContainer root) {
        root.removeAllComponents();
        try {
            LoadingDots loadingDots = new LoadingDots(getContext());
            loadingDots.setAutoPlay(true);
            loadingDots.setDotsCount(DOTS_COUNT);
            loadingDots.setDotsColor(Color.RED.getValue());
            root.addComponent(loadingDots, new ComponentContainer.LayoutConfig(
                    ComponentContainer.LayoutConfig.MATCH_CONTENT, 150));

        } catch (NotExistException | WrongTypeException | IOException exception) {
            LogUtil.error("LoadingDots" , exception.getMessage());
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start:
                startAll(root);
                break;
            case ResourceTable.Id_stop:
                stopAll(root);
                break;
            case ResourceTable.Id_loaddynamic:
                loadDynamic(root);
                break;
        }
    }
}
